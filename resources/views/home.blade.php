@extends('layouts.partials.master')

@section('contentMaster')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    DEFAULT LANDING PAGE -> PLEASE GO TO THE <a href="{{ route('dashboard.documents') }}">documents (link)</a> PAGE
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
