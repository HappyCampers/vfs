@extends('layouts.partials.master')
@section('title', 'Create Member Account')
@section('contentMaster')
@if(session('created') == "true")
<div class="col-md-12">
    <div class="alert alert-success">
        <h3><i class="fa fa-check"></i> User created successfully</h3>
        <p>The user has been created and stored into the database. If you would like to view/edit the user, please use the user editor</p>
    </div>
</div>
@endif
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">Member Information</div>
        <div class="box-body">
            <form method="POST" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email (Vigilant Email)</label>
                            <input class="form-control" type="email" name="email" placeholder="Enter users email">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Members name</label>
                            <input class="form-control" type="text" name="name" placeholder="Enter members name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name="password" class="form-control" placeholder="Enter Password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Member Number</label>
                            <input type="text" name="mem_number" class="form-control" placeholder="Confirm Password">
                        </div>
                    </div>
                </div>
                <button class="btn btn-block btn-success">Create User</button>
            </form>
        </div>
    </div>
</div>
@endsection