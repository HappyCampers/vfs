<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use User;
use DB;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userInfoPuller = DB::table('users')->where('id', Auth::id())->first();

        if(Auth::check() && auth()->user()->admin)
        {
            return $next($request);
        }

        return redirect('/dashboard');
    }
}
