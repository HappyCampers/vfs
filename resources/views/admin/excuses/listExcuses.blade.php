@extends('layouts.partials.master')
@section('contentMaster')
    <div class="container-fluid">
        <div class="row">
            <div class="box box-default">
                <div class="box-header with-border">View Excuse Report</div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Member Name</th>
                            <th>Function Name</th>
                            <th>Function Time</th>
                            <th>Reason</th>
                            <th>Time Stamp</th>
                            <th>Status</th>
                            <th>View Excuse</th>
                            <th>Approve or Deny</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($userExcuseReports as $excuseReport)
                            <tr>
                                <td><b>{{ $excuseReport['user_id'] }}</b></td>
                                <td>{{ $excuseReport['title'] }}</td>
                                <td>{{ $excuseReport['member_name'] }}</td>
                                <td>{{ $excuseReport['function_name'] }}</td>
                                <td>{{ $excuseReport['function_time'] }}</td>
                                <td>{{ $excuseReport['reason'] }}</td>
                                <td>{{ $excuseReport['created_at'] }}</td>
                                <td>
                                    @if($excuseReport['status'] == 1)
                                        <div class="label label-default">Not Answered</div>
                                    @elseif($excuseReport['status'] == 2)
                                        <div class="label label-success">Answered - Approved</div>
                                    @elseif($excuseReport['status'] == 3)
                                        <div class="label label-danger">Answered - Denied</div>
                                    @endif
                                </td>
                                <td>
                                    <a href="/admin/excuses/view/{{ $excuseReport['id'] }}"><button class="btn btn-sm btn-warning">View Excuse</button></a>
                                </td>
                                <td>
                                    @if($excuseReport['status'] == 1)
                                    <a href="/admin/excuses/approve/{{ $excuseReport['id'] }}"><button class="btn btn-sm btn-info">Approve</button></a>
                                    <a href="/dashboard/excuses/delete/{{ $excuseReport['id'] }}"><button class="btn btn-sm btn-danger">Deny</button></a>
                                    @elseif($excuseReport['status'] == 2 || 3)
                                    <a href="/admin/excuses/undo/{{ $excuseReport['id'] }}"><button class="btn btn-sm btn-primary">Undo</button></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @empty($useExcuseReports)
                    <div class="box-footer">
                        <span>Nothing to list...</span>
                    </div>
                @endempty
            </div>
        </div>
    </div>
@endsection