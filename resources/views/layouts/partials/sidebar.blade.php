 <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/user1-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a>
                        <a href="#"><button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Account"><i class="fa fa-user"></i></button></a>
                        <a href="{{ route('user.changePassword') }}"><button class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" data-container="body" title="Change Password"><i class="fa fa-lock"></i></button></a>
                        <a href="{{ route('user.logout') }}"><button class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></button></a>
                </a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">User Links</li>

            @if(Route::currentRouteName() == 'dashboard.home')
            <li class="active"><a href="{{ route('dashboard.home') }}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            @else
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            @endif

            @if(Route::currentRouteName() == 'dashboard.documents') <!-- Check if is documents URI for active sidebar -->
            <li class="active"><a href="{{ route('dashboard.documents')  }}"><i class="fa fa-file"></i> <span>Documents</span></a></li>
            @else
            <li><a href="{{ route('dashboard.documents') }}"><i class="fa fa-file"></i> <span>Documents</span></a></li>
            @endif

            <li class="treeview {{ (Route::currentRouteName() == "excuse.list" || Route::currentRouteName() == "excuse.create") ? "active" : "" }}">
                <a href="#"><i class="fa fa-tasks"></i> <span>Excuse Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteName() == "excuse.create" ? "active" : "" }}"><a href="{{ route('excuse.create') }}"><i class="fa fa-upload"></i> Create Excuse Report</a></li>
                    <li class="{{ Route::currentRouteName() == "excuse.list" ? "active" : "" }}"><a href="{{ route('excuse.list') }}"><i class="fa fa-eye"></i> View Excuse Reports</a></li>
                </ul>
            </li>

            @if(Auth::user()->admin == 1)
            <li class="header">Admin Links</li>
            <li class="treeview {{ (Route::currentRouteName() == "document.uploader" || Route::currentRouteName() == "document.manage") ? "active" : ""}}">
                <a href="#"><i class="fa fa-file"></i> <span>Documents Manager</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteName() == "document.uploader" ? "active" : "" }}"><a href="{{ route('document.uploader') }}"><i class="fa fa-cloud-upload"></i> Upload Documents</a></li>
                    <li class="{{ Route::currentRouteName() == "document.manage" ? "active" : "" }}"><a href="{{ route('document.manage') }}"><i class="fa fa-files-o"></i> Manage Documents</a></li>
                </ul>
            </li>
            <li class="treeview {{ (Route::currentRouteName() == "admin.createUser") || (Route::currentRouteName() == "admin.listUser") ? "active" : ""}}">
                <a href="#"><i class="fa fa-users"></i> <span>User Manager</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteName() == "admin.createUser" ? "active" : "" }}"><a href="{{ route('admin.createUser') }}"><i class="fa fa-user-plus"></i> <span>Create User</span></a></li>
                    <li class="{{ Route::currentRouteName() == "admin.listUser" ? "active" : "" }}"><a href="{{ route('admin.listUser') }}"><i class="fa fa-user-md"></i> <span>Edit Users</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-archive"></i> <span>Excuse Reports Manager</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="{{ route('admin.excuse.list') }}"><i class="fa fa-archive"></i> <span>View Current Reports</span></a></li>
                </ul>
            </li>
            @endif

            @if(Auth::user()->admin == 2)
                <li class="header">Chief Panel</li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-archive"></i> <span>Excuse Reports Manager</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class=""><a href="{{ route('admin.excuse.list') }}"><i class="fa fa-archive"></i> <span>View Current Reports</span></a></li>
                    </ul>
                </li>
            @endif

        </ul>
        <!-- /.sidebar-menu -->
    </section>