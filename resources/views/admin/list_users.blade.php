@extends('layouts.partials.master')
@section('contentMaster')
<div class="box">
    <div class="box-header with-border">Current Users</div>
    <div class="box-body">
        <table class="table table-bordered" style="table-layout: auto;">
            <thead>
                <tr>
                    <th><b>ID</b></th>
                    <th><b>NAME</b></th>
                    <th><b>EMAIL</b></th>
                    <th><b>VIGILANT NUMBER</b></th>
                    <th><b>EDIT/DELETE</b></th>
                </tr>
            </thead>
            <tbody>
            @foreach($currentUsers as $currentUser)
                <tr>
                    <td>{{ $currentUser->id }}</td>
                    <td>{{ $currentUser->name }}</td>
                    <td>{{ $currentUser->email }}</td>
                    <td><b>{{ $currentUser->number }}</b></td>
                    <td><a href="admin/users/delete/{{ $currentUser->id }}"><button class="btn btn-xs btn-danger">Delete</button></a> <a href=""><button class="btn btn-xs btn-info">Edit</button></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection