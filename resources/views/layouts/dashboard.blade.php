@extends('layouts.partials.app')
@section('content')
    <div class="container">

        <div class="row">
            <div class="page-header">
                <h1>GNVFS - EXAMPLE HEADER</h1>
            </div>
            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#"><i class="fa fa-home fa-fw"></i>Home</a></li>
                    <li><a href="{{ route('dashboard.documents') }}"><i class="fa fa-file-o fa-fw"></i>Documents</a></li>
                </ul>
            </div>
            <div class="col-md-9">
                @yield('maincontent')
            </div>
        </div>
    </div>
@endsection