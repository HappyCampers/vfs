<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExcuseReportMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excuse_reports', function (Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();

            $table->string('title');
            $table->string('member_name');
            $table->string('function_name');
            $table->smallInteger('function_time');
            $table->string('reason');
            $table->string('description');
            $table->string('date');

            $table->integer('status')->default(1);

            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excuse_reports');
    }
}
