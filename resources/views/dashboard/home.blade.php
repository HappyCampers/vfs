@extends('layouts.partials.master')
@section('title', 'Dashboard')
@section('contentMaster')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <div class="info-box bg-red">
                <div class="info-box-icon"><i class="fa fa-flag"></i></div>
                <div class="info-box-content">
                    <div class="info-box-text">Excuse Reports</div>
                    <div class="info-box-number">10</div>
                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <div class="progress-description">5 Not answered</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection