<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">GNVFS Admin Portal</a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Test User <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                @if(Request::is('admin/dashboard'))
                <li>
                    <a href="index.html" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                @else
                <li>
                    <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                @endif
                @if(Request::is('admin/documents/uploader'))
                <li>
                    <a href="#"><i class="fa fa-upload fa-fw"></i> Documents<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ Route('document.uploader') }}" class="active"><i class="fa fa-upload fa-fw"></i> Upload Documents</a>
                        </li>
                        <li>
                            <a href="{{ Route('document.manage')  }}">View Documents</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                @else
                <li>
                    <a href="#"><i class="fa fa-upload fa-fw"></i> Documents<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ Route('document.uploader') }}"><i class="fa fa-upload fa-fw"></i> Upload Documents</a>
                        </li>
                        <li>
                            <a href="{{ Route('document.manage') }}">View Documents</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                @endif
                <li>
                    <a href="#"><i class="fa fa-user fa-fw"></i> Users</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>