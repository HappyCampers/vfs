<?php

namespace App\Http\Middleware;

use Closure;
use App\Excuses;
use Auth;

class ExcuseReportViewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Grab essential variables to make this middleware work
        $pageId = $request->id;
        $userId = Auth::id();

        $excuseReport = Excuses::where('id', $pageId)->first();

        if($excuseReport->user_id == $userId)
        {
            return $next($request);
        }
        else
        {
            return redirect()->route('errors.404');
        }
    }
}
