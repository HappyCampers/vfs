@extends('layouts.partials.master')
@section('title', 'Document Uploader')
@section('contentMaster')
    <div class="panel panel-default">
        <div class="panel-heading">Document Uploader</div>
        <div class="panel-body">
            <form method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Document Title</label>
                    <input type="text" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label>Document Description</label>
                    <input type="text" name="description" class="form-control" size="4">
                </div>
                <div class="form-group">
                    <label class="btn btn-default btn-file">
                        Browse <input name="document" type="file" hidden>
                    </label>
                </div>
                <button class="btn btn-block">Upload Document</button>
            </form>
        </div>
    </div>
    @endsection