@extends('layouts.partials.master')
@section('title', 'Document Manager')
@section('contentMaster')
@foreach($documents as $document)
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="/admin/delete/{{ $document->id }}" class="pull-right"><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button></a>

        <a href="/admin/edit/{{ $document->id }}" class="pull-right" style="margin-right: 5px;"><button class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> Edit</button></a>
        {{ $document->title }}
    </div>
    <div class="panel-body">
        {{ $document->description }}
    </div>
</div>
@endforeach
@endsection