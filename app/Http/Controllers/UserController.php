<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /*****************************
     * CHANGE PASSWORD FUNCTIONS *
    ******************************/
    public function getChangePassword()
    {
        return view('dashboard.user.changePassword');
    }

    public function postChangePassword(Request $request)
    {
        if($request['password'] == $request['password_confirm'])
        {
            $this->validate($request,
                [
                   'password' => 'required|alpha_num|min:6|max:50|filled',
                ]);

            $user = User::where('id', Auth::id())->first();
            $user->password = bcrypt($request['password']);
            $user->save();

            return redirect()->route('user.changePassword')->with('passwordSuccess', 'Password changed successfully!');
        }
        else
        {
            return redirect()->route('user.changePassword')->with('passwordStatus', 'Password must match!');
        }

    }
}
