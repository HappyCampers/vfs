<!doctype HTML>
<html>
<head>
    @include('layouts.partials.header')
    @section('headerTitle')
        @yield('masterHeaderTitle')
    @endsection
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        @include('layouts.partials.navbar')
    </header>
</div>
<aside class="main-sidebar">
    @include('layouts.partials.sidebar')
</aside>
<div class="content-wrapper">
    <section class="content-header">
        <h1>@yield('title')</h1>
    </section>
    <section class="content container-fluid">
        @yield('contentMaster')
    </section>
</div>
</body>
<footer class="main-footer">
    @include('layouts.partials.footer')
</footer>
</html>