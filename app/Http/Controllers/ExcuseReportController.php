<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Excuses;
use Validator;
use Auth;
use Carbon\Carbon;
use DB;

class ExcuseReportController extends Controller
{
    public function getExcuseCreator()
    {
        return view('dashboard.excuses.createExcuse');
    }

    public function postExcuseCreator(Request $request)
    {
        $this->validate($request,
            [
                'title' => 'required|min:5|max:50',
                'function_name' => 'required|min:5|max:50',
                'function_time' => 'required|min:4|max:4',
                'description' => 'required|min:12|max:512',
                'reason' => 'required',
                'date' => 'required|date_format:m/d/Y'
            ]);

        $excuseCreate = Excuses::create(
            [
                'user_id' => Auth::id(),
                'title' => $request['title'],
                'member_name' => Auth::user()->name,
                'function_name' => $request['function_name'],
                'function_time' => $request['function_time'],
                'reason' => $request['reason'],
                'description' => $request['description'],
                'date' => $request['date'],

                'created_at' => time()
            ]);

        $excuseCreate->save();

        return redirect()->route('excuse.list');
    }

    public function getExcuseList()
    {
        $userExcuseReports = Excuses::where('user_id', Auth::id())->get();

        return view('dashboard.excuses.listExcuses', compact('userExcuseReports'));
    }

    public function getRequestedExcuse($id)
    {
        $requestedExcuse = Excuses::where('id', $id)->first();

        return view('dashboard.excuses.viewExcuse', compact('requestedExcuse'));
    }

    public function getDeleteExcuse($id)
    {
        Excuses::where('id', $id)->delete();

        return redirect()->route('excuse.list');
    }

}
