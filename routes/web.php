<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
    return redirect('/dashboard/');
});

Auth::routes();

Route::get('/logout', 'DashboardController@doLogout')->name('user.logout');

Route::get('/404', 'ErrorController@get404')->name('errors.404');

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function()
    {
        Route::group(['prefix' => 'user'], function()
        {
            Route::get('/changePassword', 'UserController@getChangePassword')->name('user.changePassword');
            Route::post('/changePassword', 'UserController@postChangePassword');
        });

        Route::get('/', 'DashboardController@getIndex')->name('dashboard.home');

        Route::get('/documents', 'DashboardController@getDocuments')->name('dashboard.documents');

        Route::group(['prefix' => 'excuses'], function()
        {
            Route::group(['middleware' => 'requestedExcuse'], function()
            {
                Route::get('/view/{id}', 'ExcuseReportController@getRequestedExcuse')->name('excuse.request');

                Route::get('/delete/{id}', 'ExcuseReportController@getDeleteExcuse');
            });

            Route::get('/list', 'ExcuseReportController@getExcuseList')->name('excuse.list');

            Route::get('/create', 'ExcuseReportController@getExcuseCreator')->name('excuse.create');
            Route::post('/create', 'ExcuseReportController@postExcuseCreator');

        });


    });

Route::group(['prefix' => '/admin', 'middleware' => 'admin'], function()
{
    Route::get('/', 'AdminController@getIndex')->name('admin');

    Route::group(['prefix' => '/documents'], function()
    {
        Route::get('/uploader', 'AdminController@getDocumentUploader')->name('document.uploader');
        Route::post('/uploader', 'AdminController@postDocumentUploader');

        Route::get('/manage', 'AdminController@getDocumentManager')->name('document.manage');
    });

    Route::get('/delete/{id}', 'AdminController@postDocumentDelete');

    Route::group(['prefix' => 'users'], function()
    {
        Route::get('create', 'AdminController@getCreateUser')->name('admin.createUser');
        Route::post('create', 'AdminController@postCreateUser');

        Route::get('list', 'AdminController@getListUsers')->name('admin.listUser');
        Route::post('list', 'AdminController@postListUser');

        Route::get('/delete/{id}', 'AdminController@getRemoveUser');

        Route::get('/edit/{id}', 'AdminController@getEditUser');
    });

    Route::group(['prefix' => 'excuses'], function()
    {
        Route::get('/list', 'AdminController@getListExcuses')->name('admin.excuse.list');
        Route::get('/view/{id}', 'AdminController@adminViewExcuse');

        Route::get('/approve/{excuseId}', 'AdminController@approveExcuse');
        Route::get('/deny/{excuseId}', 'AdminController@denyExcuse');
        Route::get('/undo/{excuseId}', 'AdminController@undoExcuse');
    });

});


Route::group(['prefix' => 'chief', 'middleware' => 'chief'], function()
{
    Route::group(['prefix' => '/excuses'], function()
    {
        Route::get('/list', 'AdminController@getListExcuses')->name('chief.excuse.list');
        Route::get('/view/{id}', 'AdminController@adminViewExcuse');

        Route::get('/approve/{excuseId}', 'AdminController@approveExcuse');
        Route::get('/deny/{excuseId}', 'AdminController@denyExcuse');
        Route::get('/undo/{excuseId}', 'AdminController@undoExcuse');
    });

});
