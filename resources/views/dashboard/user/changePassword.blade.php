@extends('layouts.partials.master')
@section('')
@section('contentMaster')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-push-2 col-md-5">
            @if(session('passwordStatus'))
                <div class="alert alert-danger">
                    <h4><i class="fa fa-exclamation-triangle"></i> Error!</h4>
                    <p>Passwords must match!</p>
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <h4><i class="fa fa-exclamation-triangle"></i> Error!</h4>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            @if(session('passwordSuccess'))
                <div class="alert alert-success">
                    <h4><i class="fa fa-check-circle"></i> Success</h4>
                    <p>Password changed successfully!</p>
                </div>
            @endif
            <div class="box box-success">
                <div class="box-header with-border">Change Current Password</div>
                <div class="box-body">
                    <form method="POST" enctype="application/x-www-form-urlencoded">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="text" name="password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="text" name="password_confirm" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-block btn-success">Submit Password Change</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-push-2 col-md-3">
            <div class="box box-info">
                <div class="box-header">Password Notice</div>
                <div class="box-body">
                    <p>Any member that has not changed their password from the default password must do so now.</p>
                    <p>Using your default number password will not be an effective password and will allow anyone to log into your account and do tasks under your name.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection