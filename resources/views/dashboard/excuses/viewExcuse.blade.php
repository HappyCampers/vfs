@extends('layouts.partials.master')
@section('contentMaster')
<div class="box box-default">
    <div class="box-header with-border">
        {{ $requestedExcuse['title'] }} - {{ $requestedExcuse['reason'] }}
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                @if($requestedExcuse['status'] == 1)
                <div class="alert alert-info"><p class="text-center"><i class="fa fa-hourglass"></i> This report is <b>pending</b></p></div>
                @elseif($requestedExcuse['status'] == 2)
                <div class="alert alert-success"><p class="text-center"><i class="fa fa-check-circle"></i> This report has been <b>approved</b></p></div>
                @elseif($requestedExcuse['status'] == 3)
                <div class="alert alert-danger"><p class="text-center"><i class="fa fa-exclamation-triangle"></i> This report has been <b>denied</b></p></div>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-info">
                <div class="box-header with-border">
                    Function Name
                </div>
                <div class="box-body">
                    <p class="text-center"><b>{{ $requestedExcuse['function_name']  }}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-info">
                <div class="box-header with-border">
                    Function Time
                </div>
                <div class="box-body">
                    <p class="text-center"><b>{{ $requestedExcuse['function_time']  }}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-info">
                <div class="box-header with-border">
                    Function Date
                </div>
                <div class="box-body">
                    <p class="text-center"><b>{{ $requestedExcuse['date']  }}</b></p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    Description
                </div>
                <div class="box-body">
                    {{ $requestedExcuse['description'] }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection