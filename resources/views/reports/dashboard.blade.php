@extends('layouts.dashboard')
@section('maincontent')
            <div class="panel panel-default">
                <div class="panel-heading">Current Excuse Reports<a href="{{ route('excuse.create') }}" class="btn btn-default btn-xs pull-right">Create new excuse report</a></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>Report ID</th>
                                <th>Vigilant ID</th>
                                <th>Date Proccessed</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($excuseReports as $excuseReport)
                                <tr>
                                    <td>132749</td>
                                    <td>{{ $excuseReport->user_id }}</td>
                                    <td>{{ $excuseReport->date }}</td>
                                    <td>
                                        @if($excuseReport->status == 1)
                                        <span class="label label-default">PENDING</span>
                                            @elseif($excuseReport->status == 2)
                                        <span class="label label-success">APPROVED</span>
                                            @else
                                        <span class="label label-danger">DENIED</span>
                                        @endif
                                    </td>
                                    <td><button class="btn btn-xs btn-block btn-default">View</button></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection