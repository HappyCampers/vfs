@extends('layouts.partials.master')
@section('contentMaster')
<div class="container-fluid">
    <div class="row">
        <div class="box box-default">
            <div class="box-header with-border">View Excuse Report</div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Function Name</th>
                            <th>Function Time</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th>View or Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($userExcuseReports as $excuseReport)
                        <tr>
                            <td><b>{{ $excuseReport['user_id'] }}</b></td>
                            <td>{{ $excuseReport['title'] }}</td>
                            <td>{{ $excuseReport['function_name'] }}</td>
                            <td>{{ $excuseReport['function_time'] }}</td>
                            <td>{{ $excuseReport['reason'] }}</td>
                            <td>
                                @if($excuseReport['status'] == 1)
                                <div class="label label-default">Pending</div>
                                @elseif($excuseReport['status'] == 2)
                                <div class="label label-success">Approved</div>
                                @elseif($excuseReport['status'] == 3)
                                <div class="label label-danger">Denied</div>
                                @endif
                            </td>
                            <td>
                                <a href="/dashboard/excuses/view/{{ $excuseReport['id'] }}"><button class="btn btn-sm btn-info">View</button></a>
                                @if($excuseReport['status'] == 1)
                                <a href="/dashboard/excuses/delete/{{ $excuseReport['id'] }}"><button class="btn btn-sm btn-danger">Delete</button></a>
                                @elseif($excuseReport['status'] == 2 || 3)
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @empty($useExcuseReports)
            <div class="box-footer">
                <span>Nothing to list...</span>
            </div>
            @endempty
        </div>
    </div>
</div>
@endsection