<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GNVFS Admin Portal</title>

    <link href="{{ asset('adminassets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminassets/css/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminassets/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('adminassets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
</head>
@include('admin.layouts.sidebar')