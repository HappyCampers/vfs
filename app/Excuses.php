<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excuses extends Model
{
    protected $table = 'excuse_reports';

    protected $fillable = [
        'title',
        'user_id',
        'member_name',
        'function_name',
        'function_time',
        'reason',
        'status',
        'description',
        'date'
    ];
}
