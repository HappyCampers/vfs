@include('admin.layouts.header')
<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">@yield('title')</h1>
    </div>
        @yield('admincontent')
</div>
@include('admin.layouts.footer')
