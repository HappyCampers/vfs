<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Documents;
use Auth;
use App\User;

class DashboardController extends Controller
{
    public function getIndex()
    {
        return redirect()->route('dashboard.documents');
    }

    public function getDocuments()
    {
        $documents = Documents::all();
        $user = User::find(Auth::id());

        return view('dashboard.documents', compact('documents', 'user'));

    }

    public function doLogout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
