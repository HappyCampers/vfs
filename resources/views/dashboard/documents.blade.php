@extends('layouts.partials.master')
@section('masterHeaderTitle', 'Current Documents')
@section('title', 'Current Documents')
@section('contentMaster')
    @foreach($documents as $document)
    <div class="box box-success">
        <div class="box-header with-border">{{ $document->title }} <a class="pull-right btn btn-xs btn-default" href="/documents/{{ $document->name }}">View Document</a></div>
        <div class="box-body">
            <p>{{ $document->description }}</p>
        </div>
    </div>
    @endforeach
@endsection