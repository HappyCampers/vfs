<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Validation\Validator;
use App\Documents;
use Storage;
use File;
use App\User;
use App\Excuses;

class AdminController extends Controller
{
    public function getIndex()
    {
        return view('admin.home');
    }

    /*******************************
     * DOCUMENT UPLOADER FUNCTIONS *
     *******************************/

    public function getDocumentUploader()
    {
        return view('admin.document_uploader');
    }

    public function postDocumentUploader(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'document' => 'required|file'
        ]);

        $file = $request->file('document');

        $extension = $file->getClientOriginalExtension();
        $fileName = md5_file($request->file('document')). '.' .$extension; //MD5 filename and then extension
        Storage::disk('documents')->put($fileName, File::get($file));

        $documentUploader = Documents::create([
            'title' => $request['title'],
            'description' => $request['description'],
            'name' => $fileName
        ]);

        $documentUploader->save();
        return view('admin.document_uploader');

    }

    public function getDocumentManager()
    {
        $documents = Documents::all();

        return view('admin.document_manage', compact('documents'));
    }

    public function postDocumentDelete($id)
    {
        $document = Documents::find($id);
        Documents::find($id)->delete();

        Storage::disk('documents')->delete($document->name);

        return redirect()->route('document.manage');
    }

    public function postDocumentManageEdit()
    {

    }

    /******************
     * USER FUNCTIONS *
     ******************/

    public function getCreateUser()
    {
        return view('admin.create_user');
    }

    public function postCreateUser(Request $request)
    {
        $this->validate($request,
        [
            'email' => 'required',
            'name' => 'required',
            'mem_number' => 'required',
            'password' => 'required'
        ]);

        //Store the creation query into a variable to check if it was successful or not
        $userCreate = User::create(
        [
            'email' => $request['email'],
            'name' => $request['name'],
            'number' => $request['mem_number'],
            'password' => bcrypt($request['password'])
        ]);

        if(isset($userCreate))
        {
            return redirect()->route('admin.createUser')->with('created', 'true');
        }
    }

    public function getListUsers()
    {
        $currentUsers = User::get();

        return view('admin.list_users', compact('currentUsers'));
    }

    public function getRemoveUser($id)
    {
        User::where('id', $id)->delete();

        return redirect()->route('admin.listUser');

    }

    public function getEditUser($id)
    {
        $userInformation = User::where('user_id', $id)->get();

        return view('admin.edit.user', compact('userInformation'));

    }

    /***************************
     * EXCUSE REPORT FUNCTIONS *
     ***************************/

    public function getListExcuses()
    {
        $userExcuseReports = Excuses::all();

        return view('admin.excuses.listExcuses', compact('userExcuseReports'));

    }

    public function adminViewExcuse($id)
    {
        $requestedExcuse = Excuses::where('id', $id)->first();

        return view('dashboard.excuses.viewExcuse', compact('requestedExcuse'));
    }

    public function approveExcuse($excuseId)
    {
        $retrieveExcuse = Excuses::where('id', $excuseId)->first();

        $retrieveExcuse->status = 2;
        $retrieveExcuse->save();

        if(auth()->user()->admin == 1)
        {
            return redirect()->route('admin.excuse.list');
        }
        elseif(auth()->user()->admin == 2)
        {
            return redirect()->route('chief.excuse.list');
        }

    }

    public function denyExcuse($excuseId)
    {
        $retrieveExcuse = Excuses::where('id', $excuseId)->first();

        $retrieveExcuse->status = 3;
        $retrieveExcuse->save();

        if(auth()->user()->admin == 1)
        {
            return redirect()->route('admin.excuse.list');
        }
        elseif(auth()->user()->admin == 2)
        {
            return redirect()->route('chief.excuse.list');
        }
    }

    public function undoExcuse($excuseId)
    {
        $retrieveExcuse = Excuses::where('id', $excuseId)->first();

        $retrieveExcuse->status = 1;
        $retrieveExcuse->save();

        if(auth()->user()->admin == 1)
        {
            return redirect()->route('admin.excuse.list');
        }
        elseif(auth()->user()->admin == 2)
        {
            return redirect()->route('chief.excuse.list');
        }

    }



}
