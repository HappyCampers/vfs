<!-- Main Footer -->
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <b>Version:</b> 0.0.1b
        <small>- Built by 822</small>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="http://vigilantfd.com">Vigilant Fire Company</a>.</strong> All rights reserved.


<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3.1.1 -->
<script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/adminlte.min.js') }}"></script>

<script type="text/javascript">
    $(function()
    {
        $('[data-toggle="tooltip"]').tooltip();
    })
</script>