@extends('layouts.partials.master')
@section('title', 'Create Excuse Report')
@section('contentMaster')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="box box-default">
                    <div class="box-header with-border">Create Excuse Report</div>
                    <div class="box-body">
                        <form method="POST" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <h4><i class="fa fa-exclamation-triangle"></i> Error!</h4>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Report Title</label>
                                        <input class="form-control" type="text" name="title" placeholder="Ex: Can't make drill">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Function Name</label>
                                        <input class="form-control" type="text" name="function_name" placeholder="Ex: Bailout Drill">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Time of Function</label>
                                    <input class="form-control" type="text" name="function_time" placeholder="Ex: 0100">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Reason: <small>In a few sentences please explain the reason for submitting this excuse report</small></label>
                                        <textarea class="form-control" rows="3" name="description" placeholder="I have a family event to attend..."></textarea>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Reason</label>
                                        <select class="form-control" name="reason">
                                            <option>Select an option</option>
                                            <option value="Illness">Illness</option>
                                            <option value="Business">Business</option>
                                            <option value="Religious Observances">Religious Observances</option>
                                            <option value="School Work">School Work</option>
                                            <option value="Military">Military</option>
                                            <option value="Functions of other Organizations">Functions of other Organizations</option>
                                            <option value="Patriotic Observances">Patriotic Observances</option>
                                            <option value="Family Obligations">Family Obligations</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input class="form-control" data-provide="datepicker" name="date" >
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-block btn-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-info">
                    <div class="box-header with-border">Excuse Report Helper</div>
                    <div class="box-body">
                        <code>ALL FIELDS ARE REQUIRED</code>
                        <hr>
                        <h4>Member Information:</h4>
                        <p>Name: <b>{{ Auth::user()->name }}</b></p>
                        <p>Number: <b>{{ Auth::user()->number }}</b></p>
                        <hr>
                        <h4>How to create an excuse report</h4>
                        <p>With this online excuse report creator, sending in and viewing excuse reports couldn't be easier! With this in mind, you should always be submitting excuse reports for valid reasons</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection